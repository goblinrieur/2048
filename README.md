# 2048

I had some time to kill while sitting on airplanes during my summer vacation.
I had been playing 2048 on my phone.
It's a fun game; think a cross between tetris and sudoku.
I thought it would be fun to recreate it in Forth.
It seemed a good opportunity to exercise the Forth concepts I have learned so far.

At the time of this writing, it's playable,
but incomplete.
For example, if you fill the grid,
`new` will get stuck in an infinite loop.
Also, the game doesn't know whether you've won or lost.
Also, the game will place a new tile even if you make an invalid move.

So, there are a number of things that need to be finished.
I've had fun writing this so far,
but I need to move on for now.

[![asciicast](https://asciinema.org/a/9rSioiFnpBDtFWqmDVZyIWuxg.svg)](https://asciinema.org/a/9rSioiFnpBDtFWqmDVZyIWuxg)

All work until this fork is done by : [Dave Bucklin](https://gitlab.com/davebucklin/2048)

# My version Below

## goals : 
- First goal is to adapt that game to azerty keyboard _(will be easiest stuff)_
	- [x] Specify keyboard is bad approach : choose to use vi/vim directions management
	- [x] Found issue there is no *"no more move possible detection"*

	`$7F90B80D0C98 box@` here we go => if no more cases are free it must check if any operations are possible
		
	- [x] Found issue on the scoring 
	- [x] Found issue there are only tow's & no four randomly given value

	_full rewriting with general purpose algorithm to keep it free to maintain_

	- Should be cool to add a special effect on 2048 score 

[VERY FAST DEMO video](https://youtu.be/zshrpCkCYbA)

Work In Progress branch until there is : bug1

# Here I tag project v0.1

- Then I will to some understanding & learning adaptations
- Why not if I a am motivated enough plug it in a multiple shell games compilation

# Here we tag v1.4.0

- Now playable with cursor / arrow keys
- Have title/help header on start
- Added to-upper, miplace, curseur words
- Colorized game
- Autodoc generated

